<?php
  session_start();

  // set db
  if (isset($_COOKIE['userId']) && $_COOKIE['userId']) {
    $userId = (int)($_COOKIE['userId']);
  } else {
    $userId = uniqid();
    setcookie('userId', $userId, time() + (1 * 365 * 24 * 60 * 60));    
  }

  $user = 'root';
  $password = 'root';
  $host = 'localhost';
  $database = 'mySuperCalendarDB';

  $db = new PDO(
    'mysql:host='. $host . ';dbname=' . $database,
    $user, $password,
    [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]
  );

  $reponse = $db->query('SELECT * FROM events ORDER BY eventDate, userId');
  while ($event = $reponse->fetch()) {
    $events[] = $event;
  }
?>

<html lang="fr" class="">
<head>
  <meta charset="utf-8">
  <meta name="robots" content="noindex">
  <style class="cp-pen-styles" type="text/css">

  </style>

  <link rel="stylesheet" href="style.css">

  <title>Calendar</title>
</head>

<body>
  <div class="wrapp">
    <div class="flex-calendar">
      <?php
      $days = array(
        'L',
        'M',
        'M',
        'J',
        'V',
        'S',
        'D'
      );

      $months = array(
        'Janvier',
        'Février',
        'Mars',
        'Avril',
        'Mai',
        'Juin',
        'Juillet',
        'Août',
        'Septembre',
        'Octobre',
        'Novembre',
        'Décembre'
      );

      if (!isset($_GET['month'])) {
        $monthSetted = date('m');
      } else {
        $monthSetted = $_GET['month'];
      }

      if (!isset($_GET['year'])) {
        $yearSetted = date('Y');
      } else {
        $yearSetted = $_GET['year'];
      }

      if ($monthSetted == 0) {
        $yearSetted -= 1;
        $monthSetted = 12;
      } else if ($monthSetted == 13) {
        $yearSetted += 1;
        $monthSetted = 1;
      }


      if (isset($_REQUEST['event'], $_REQUEST['date'], $_REQUEST['message']) && $_REQUEST['event'] && $_REQUEST['date'] && $_REQUEST['message']) {
        $request = array('-1',$_REQUEST['event'], $_REQUEST['message'], "", $_REQUEST['date']);

        if (isset($_FILES['image']) && $_FILES['image']['size']) {
          $fichier = finfo_open(FILEINFO_MIME_TYPE);

          if (finfo_file($fichier, $_FILES['image']['tmp_name']) == 'image/jpeg') {
            $uuid = uniqid();
            move_uploaded_file($_FILES['image']['tmp_name'], 'img' . $uuid . '.jpg'); // probème duplication (faire changer le nom)

            $filePath = 'img' . $uuid . '.jpg';

            $request[3] = $filePath;
          }
        }

        // save to db
        $sql = 'INSERT INTO events(userId, title, content, imgName, eventDate) 
                VALUE (:userId, :title, :content, :imageName, :eventDate)';

        $res = $db->prepare(
          $sql,
          array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY)
        );

        $res->execute(array(
          ':userId' => $userId,
          ':title' => $_REQUEST['event'],
          ':content' => $_REQUEST['message'],
          ':imageName' => 'img'. $uuid . '.jpg',
          ':eventDate' => $_REQUEST['date']
        )) OR die('error');

        $events[] = $request;
      }

      ?>

      <div class="month">
        <?php
        echo '<a class="arrow visible" href="http://localhost:8888/myCalendar.php?month=' . ($monthSetted - 1) . '&year=' . $yearSetted . '"></a>';
        ?>

        <?php
        for ($i = 0; $i < 12; $i++) {
          if (($i + 1) == $monthSetted) {
            echo '<div class="label">' . $months[$i] . ' ' . $yearSetted . '</div>';
          }
        }
        ?>

        <?php
        echo '<a class="arrow visible" href="http://localhost:8888/myCalendar.php?month=' . ($monthSetted + 1) . '&year=' . $yearSetted . '"></a>';
        ?>

      </div>

      <div class="week">

        <?php
        foreach ($days as $wday) {
          echo '<div class="day">' . $wday . '</div>';
        }
        ?>
      </div>

      <div class="days">

        <?php

        $firstDay = date('w', strtotime('01-' . $monthSetted . '-' . $yearSetted));
        $dayOutcount = $firstDay - 1;

        if ($dayOutcount < 0) {
          $dayOutcount = 6;
        }

        $monthDayCount = cal_days_in_month(CAL_GREGORIAN, $monthSetted, $yearSetted);

        for ($i = 0; $i < $monthDayCount + $dayOutcount + $emptyDay; $i++) {
          if ($i >= $dayOutcount && $i < $dayOutcount + $monthDayCount) {
            $index = $i + 1 - $dayOutcount;

            if (date('d') == $index && $yearSetted == date('Y') && $monthSetted == date('m')) {
              echo '<div class="day selected"><div class="number">' . ($i + 1 - $dayOutcount) . '</div></div>';
            } else {
              $divToAdd = '<div class="day';

              foreach ($events as $event) {
                $eventDate = $event[4];
                $year = date('Y', strtotime($eventDate, time()));
                $month = date('m', strtotime($eventDate, time()));
                $day = date('d', strtotime($eventDate, time()));

                if ($year == $yearSetted && $month == $monthSetted && $day == $index) {
                  $divToAdd .= ' event';
                }
              }

              $divToAdd .= '"><div class="number">' . ($i + 1 - $dayOutcount) . '</div><div class="show">';

              foreach ($events as $event) {
                $eventDate = $event[4];
                $year = date('Y', strtotime($eventDate, time()));
                $month = date('m', strtotime($eventDate, time()));
                $day = date('d', strtotime($eventDate, time()));

                if ($year == $yearSetted && $month == $monthSetted && $day == $index) {
                  $divToAdd .= '<p class="eventTitle">' . $event[1] . '</p>';
                  if (file_exists($event[3])) {
                    $divToAdd .= '<img class="eventImage" src="' . $event[3] . '"></img>';
                  } else {
                    $divToAdd .= '<p>img error</p>';
                  }
                  $divToAdd .= '<p class="eventContent">' . $event[2] . '</p>';
                }
              }

              $divToAdd .= '</div></div>';
              echo $divToAdd;
            }
          } else {
            echo '<div class="day out"><div class="number"></div> </div>';
          }
        }
        ?>
      </div>

      <form method="post" action="myCalendar.php" enctype="multipart/form-data">
        <p>
          <label for="event">event</label>
          <input type="event" name="event" id="event" value="">
        </p>
        <p>
          <label for="date">date</label>
          <input type="date" name="date" id="date" value="">
        </p>
        <p>
          <label for="message">message</label>
          <input type="text" name="message" id="message" value="">
        </p>

        <p>
          <label for="image">image</label>
          <input type="file" name="image" id="image" value="">
        </p>

        <p>
          <button type="submit">Ajouter</button>
        </p>
      </form>

      <div class="thisMonth">
        <h2>Résumé du mois</h2>
        <ul>
          <?php
          foreach ($events as $event) {
            $eventDate = $event[4];
            $year = date('Y', strtotime($eventDate, time()));
            $month = date('m', strtotime($eventDate, time()));
            $day = date('d', strtotime($eventDate, time()));

            if ($year == $yearSetted && $month == $monthSetted) {
              echo '<li>' . $event[4] . ' : ' . $event[1] . '</li>';
            }
          }
          ?>
        </ul>
      </div>

</body>

</html>